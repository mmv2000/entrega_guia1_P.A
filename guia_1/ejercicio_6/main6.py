#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Cuenta_Cliente import Cuenta_Cliente

# se muestran los datos del cliente
print("Datos de la cuenta: ")
# Se interactua con el usuario
nombreCliente = input("NOMBRE CLIENTE: ")
numeroCuenta = int(input("NUMERO CUENTA: "))
saldoCliente = int(input("SALARIO CLIENTE:"))
tipoInteres = float(input("TIPO INTERES: "))

# Se añade el objeto
datos_cliente = Cuenta_Cliente(nombreCliente, numeroCuenta,
                               saldoCliente, tipoInteres)
datos_cliente.depositoCliente(saldoCliente)
datos_cliente.retiroDinero(saldoCliente)
datos_cliente.impresionDatos(nombreCliente, numeroCuenta, saldoCliente)
