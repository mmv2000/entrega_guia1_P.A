#!/usr/bin/env python
# -*- coding: utf-8 -*-


# class
class Cuenta_Cliente():

    # datos de la cuenta e interes
    def __init__(self, nombre, cuenta, saldo, tipoInteres):
        self.__nombre = nombre
        self.__cuenta = cuenta
        self.__saldo = saldo
        self.__tipoInteres = tipoInteres

    # actualiza el saldo del titular
    def set_nombre(self, nombre):
        self.__nombre = nombre

    # muestra el nombre del titular
    def get_nombre(self):
        return self.__nombre

    # muestra el saldo del titular
    def get_saldo(self):
        return self.__saldo

    # actualiza el saldo inicial luego de una accion realizada.
    def set_saldo(self):
        self.__saldo = saldo

    # se ingresa valor a depositar
    def depositoCliente(self, saldo):
        deposito = int(input("Ingrese deposito: "))
        aumentoSaldo = (deposito + saldo)
        print("Saldo disponible: ", aumentoSaldo)

    # se ingresa valor a retirar
    def retiroDinero(self, saldo):
        retiro = int(input("Ingrese retiro: "))
        saldoRestante = (saldo - retiro)
        print("Saldo disponible: ", saldoRestante)

    # print datos
    def impresionDatos(self, nombre, cuenta, saldo):
        print("CLIENTE: ", nombre)
        print("CUENTA: ", cuenta)
        print("SALDO: ", saldo)
