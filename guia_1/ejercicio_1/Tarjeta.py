#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#clase definida
class Tarjeta():

    # constructor de la clase
    def __init__(self, nombre, saldo):
        self.__nombre = nombre #atributo
        self.__saldo = 0

    # al sumar a la cuenta
    def cuenta_suma(self, saldo):
        self.__saldo = self.__saldo + saldo

    # al restar la cuenta
    def cuenta_resta(self, saldo):
        self.__saldo = self.__saldo - saldo

    # muestra el nombre del titular
    def get_nombre(self):
        return self.__nombre

    # actualiza el saldo del titular
    def set_nombre(self, nombre):
        self.__nombre = nombre

    # muestra el saldo del titular
    def get_saldo(self):
        return self.__saldo

    # actualiza el saldo inicial luego de una compra.
    def set_saldo(self):
        self.__saldo = saldo
