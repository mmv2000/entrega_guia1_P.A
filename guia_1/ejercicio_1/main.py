#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# se importa archivo de clase
from Tarjeta import Tarjeta

# main
if __name__ == '__main__':
    # interaccion con el usuario
    persona = input("Ingrese su nombre:")
    saldo_inicial = int(input("Ingrese saldo inicial:"))
    resta_saldo = int(input("Ingrese valor de la compra efectuada:"))
    persona_final = Tarjeta(persona, saldo_inicial)
    persona_final.cuenta_suma(saldo_inicial)
    persona_final.cuenta_resta(resta_saldo)
    #print
    print("El saldo actual de {0} es de: {1}".format(persona_final.get_nombre(),
                                                     persona_final.get_saldo()))
