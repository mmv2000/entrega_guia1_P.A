#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Cuenta_Amigos():

     # Se crea el constructor
     def __init__(self, amigos, valorCuenta, propina,
				 tasa_interes, cuanto_tiene_amigo):
		self.__amigos = amigos
		self.__valorCuenta = valorCuenta
		self.__propina = propina
		self.__tasa_interes = tasa_interes
		self.__cuanto_tiene_amigo = cuanto_tiene_amigo

	# Manipular el dato
	def set_valorCuenta(self, valorCuenta):
		self.__valorCuenta = valorCuenta

	# Calcular lo total
	def CalcularConsumo(self, valorCuenta, tasa_interes, propina):
		print("La cuenta es: ", self.__valorCuenta)
		total_pagar = ((self.__valorCuenta * tasa_interes) * propina)
		print("La cuenta final sera:", total_pagar)
		# Aqui se modifica el valorCuenta
		self.__set_valorCuenta = valorCuenta

	# Se muestra el valor cambiado
	def get_valorCuenta(self):
		return self.valorCuenta

	# Se calcula lo que cada uno cancela
	def cancelarIndividual(self, valorCuenta, amigos):
		pago_cada_uno = (self.__valorCuenta / amigos)
		print("Cada uno paga: ", pago_cada_uno)

	# Se efectua el prestamo si el dinero no alcanza
	def prestar(self, valorCuenta, cuanto_tiene_amigo):
		if(cuanto_tiene_amigo < valorCuenta):
			print("Se le prestara")
		else:
			print("Tienes dinero")
