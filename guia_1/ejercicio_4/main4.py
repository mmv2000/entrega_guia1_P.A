#!/usr/bin/env python
# -*- coding: utf-8 -*-

# se importa la clase
from Cuenta_Amigos import Cuenta_Amigos

# Se produce la interaccion con el usuario
amigos = int(input("cuantos amigos son:"))
valorCuenta = int(input("valor cuenta:"))
propina = int(input("Añade valor de la propina:"))
tasa_interes = int(input("Añade tasa interes:"))
cuanto_tiene_amigo = int(input("Cuanto tienes de dinero:"))

# metodos para el ejercicio
calculo_dinero = Cuenta_Amigos(amigos, valorCuenta, propina,
                               tasa_interes, cuanto_tiene_amigo)
calculo_dinero.set_valorCuenta(valorCuenta)
calculo_dinero.CalcularConsumo(valorCuenta, tasa_interes, propina)
calculo_dinero.CancelarIndividual(valorCuenta, amigos)
calculo_dinero.Prestar(valorCuenta, cuanto_tiene_amigo)
