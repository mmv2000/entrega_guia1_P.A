#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Biblioteca:

    def __init__(self, disponibles, prestados):
        self.__disponibles = 0
        self.__prestados = 0
        #self.__ejemplares = 0

    def get_disponibles(self):
        return self.__disponibles

    def set_disponibles(self, disponibles):
        self.__disponibles = disponibles

    def get_prestados(self):
        return self.__prestados

    def set_prestados(self, prestados):
        self.__prestados = prestados
