#!/usr/bin/env python3
# -*- coding:utf-8 -*-

class Libro():

    def __init__(self, titulo, autor):
        self.__titulo = titulo
        self.__autor = autor
        #self.__ejemplares = 0

    def get_titulo(self):
        return self.__titulo

    def set_titulo(self, titulo):
        self.__titulo = titulo

    def get_autor(self):
        return self.__autor

    def set_autor(self, autor):
        self.__autor = autor

    #def agregar_libro(self, titulo, autor):
