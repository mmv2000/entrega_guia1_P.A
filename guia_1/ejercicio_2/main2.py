#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Libro import Libro
from Biblioteca import Biblioteca


if __name__ == '__main__':

    print("BIENVENIDO A LA BIBLIOTECA")

    titulo_libro = input("Ingrese el titulo del libro:")
    autor_libro = input("Ingrese al autor:")
    ejemplares_disponibles = int(input("Ingrese numero de ejemplares:"))
    libros_prestados = int(input("Ingrese cantidad de libros prestados: "))

    libro = Libro(titulo_libro, autor_libro)
    biblioteca = Biblioteca(ejemplares_disponibles, libros_prestados)
    libro.set_titulo(titulo_libro)
    libro.set_autor(autor_libro)
#    libro.agregar_libro(titulo, autor)
    biblioteca.set_disponibles(ejemplares_disponibles)
    biblioteca.set_prestados(libros_prestados)


    print("El titulo del libro es:{0} del autor: {1}".format(libro.get_titulo(),
                                                             libro.get_autor()))
    print("Ejemplares diponibles: {0} y prestados: {1}".format(biblioteca.get_disponibles(),
                                                               biblioteca.get_prestados()))
