#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# se importa la clase
from Numero import Numero


def parte1(l_num):
    l_final = []

    for i in range(len(l_num)):
        if i == 0:
            l_final.append(l_num[i])
        else:
            if l_num[i] + l_final[i-1] > 9:
                x = (l_num[i] + l_final[i-1]) % 10
                l_final.append(x)
            else:
                l_final.append(l_num[i] + l_final[i-1])

    return l_final


def parte2(l_num):
    l_final = []
    for i in range(len(l_num)):
        l_final.append(7 + l_num[i])

    return l_final


def parte3(l_num):
    temp = l_num[2]
    l_num[2] = l_num[0]
    l_num[0] = temp

    temp = l_num[3]
    l_num[3] = l_num[1]
    l_num[1] = temp

    return l_num


if __name__ == "__main__":
    n = input("Ingrese numero: ")
    l_num = []
    l_num2 = []
    for i in range(len(n)):
        l_num.append(int(n[i]))

    punto_1 = parte1(l_num)
    punto_2 = parte2(l_num)

    for i in range(len(l_num)):
        l_num2.append(punto_2[i])

    punto_3 = parte3(l_num2)

    numero = Numero(punto_1, punto_2, punto_3)
    print(numero.getParte1())
    print(numero.getParte2())
    print(numero.getParte3())
