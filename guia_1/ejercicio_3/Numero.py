#! /usr/bin/env python3
# -*- coding:utf-8 -*-


class Numero:

    def __init__(self, punto_1, punto_2, punto_3):
        self.__parte1 = punto_1
        self.__parte2 = punto_2
        self.__parte3 = punto_3

    def getParte1(self):
        return self.__parte1

    def getParte2(self):
        return self.__parte2

    def getParte3(self):
        return self.__parte3
